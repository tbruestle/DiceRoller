﻿using System;
using System.Data;

namespace DiceRoller.Data
{
    public abstract class DataAccessBase
    {
        #region "Connection"
            public virtual string ConnectionString
            {
                get { return ""; }
            }
        #endregion
        #region "Get Table"
            public DataTable GetTable(string tablename)
            {
                switch (tablename.ToLower())
                {
                    case "pffeat":
                        return GetTablePFFeat();
                    default:
                        throw new NotImplementedException();
                }
            }
            public abstract DataTable GetTablePFFeat();
        #endregion
        #region "Get Row"
            public DataRow GetRow(string tablename, int id)
            {
                switch (tablename.ToLower())
                {
                    case "pffeat":
                        return GetRowPFFeat(id);
                    default:
                        throw new NotImplementedException();
                }
            }
            public abstract DataRow GetRowPFFeat(int id);
        #endregion
        #region "Update Row"
            public bool UpdateRow(string tablename, int id, DataRow dr)
            {
                switch (tablename.ToLower())
                {
                    case "pffeat":
                        return UpdateRowPFFeat(id, dr);
                    default:
                        throw new NotImplementedException();
                }
            }
            public abstract bool UpdateRowPFFeat(Int32 id, DataRow dr);
        #endregion
        #region "Create Row"
            public bool CreateRow(string tablename, int id, DataRow dr)
            {
                switch (tablename.ToLower())
                {
                    case "pffeat":
                        return CreateRowPFFeat(id, dr);
                    default:
                        throw new NotImplementedException();
                }
            }
            public abstract bool CreateRowPFFeat(Int32 id, DataRow dr);
        #endregion
        #region "Delete Row"
            public bool DeleteRow(string tablename, int id)
            {
                switch (tablename.ToLower())
                {
                    case "pffeat":
                        return DeleteRowPFFeat(id);
                    default:
                        throw new NotImplementedException();
                }
            }
            public abstract bool DeleteRowPFFeat(Int32 id);
        #endregion
    }
}