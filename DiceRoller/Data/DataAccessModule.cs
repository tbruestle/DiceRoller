﻿namespace DiceRoller.Data
{

    static class DataAccessModule
    {
        static DataAccessBase m_DataAccessBase = null;
        static string m_DatabaseType = System.Configuration.ConfigurationManager.AppSettings.Get("DatabaseType");
        public static DataAccessBase DataAccess
        {
            get
            {
                if (m_DataAccessBase == null)
                {
                    switch (m_DatabaseType)
                    {
                        case "XML":
                            m_DataAccessBase = new DataAccessXML();
                            break;
                        case "Hardcoded":
                            m_DataAccessBase = new DataAccessHardcoded();
                            break;
                        case "SQL":
                            m_DataAccessBase = new DataAccessSQL();
                            break;
                    }


                }
                return m_DataAccessBase;
            }
        }
    }
}