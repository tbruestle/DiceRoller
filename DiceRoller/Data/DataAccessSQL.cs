﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace DiceRoller.Data
{
    public class DataAccessSQL : DataAccessBase
    {
        #region "Connection"
        SqlConnection conn = null;
        public override string ConnectionString
        {

            get { return ConfigurationManager.AppSettings.Get("SQLConnectionString"); }
        }
        public SqlConnection Connection
        {
            get
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                return conn;
            }
        }
        #endregion
        #region "PFFeat"
        #region "Get Table"
        public override DataTable GetTablePFFeat()
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("[dbo].[P_GetTablePFFeat]");

                cmd.Connection = Connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = 0;

                //Fill the dataset
                adapter.Fill(ds);
                Connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region "Get Row"
        public override DataRow GetRowPFFeat(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("[dbo].[P_GetRowPFFeat]");

                cmd.Parameters.Add(new SqlParameter("id", SqlDbType.Int));
                cmd.Parameters["id"].Value = id;

                SqlConnection connection = new SqlConnection(ConnectionString);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = -1;

                //Fill the dataset
                adapter.Fill(ds);
                connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                if (ds.Tables[0].Rows.Count == 0)
                    return null;
                return ds.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region "Update Row"
        public override bool UpdateRowPFFeat(int id, DataRow dr)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region "Create Row"
        public override bool CreateRowPFFeat(int id, DataRow dr)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region "Delete Row"
        public override bool DeleteRowPFFeat(int id)
        {
            throw new NotImplementedException();
        }
        #endregion
        #endregion
    }
}