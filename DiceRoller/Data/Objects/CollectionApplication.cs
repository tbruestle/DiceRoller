﻿using System;

namespace DiceRoller.Data.Objects
{
    public class CollectionApplication : CollectionRoot
    {
        #region "Constructors"
            public CollectionApplication()
            {
            }
        #endregion
        #region "Member Variables"
            PFFeatTable m_PFFeattable = null;
        #endregion
        #region "Properties"
            public PFFeatTable PFFeatTable
            {
                get
                {
                    try
                    {
                        if (m_PFFeattable == null)
                        {
                            m_PFFeattable = new PFFeatTable(this);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return m_PFFeattable;
                }
            }
            public override string TableName
            {
                get { return ""; }
            }
            public override CollectionApplication Application
            {
                get { return this; }
            }
        #endregion
    }

}