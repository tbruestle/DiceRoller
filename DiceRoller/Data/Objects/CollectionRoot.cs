﻿namespace DiceRoller.Data.Objects
{
    public abstract class CollectionRoot
    {
        public abstract string TableName { get; }
        public abstract CollectionApplication Application { get; }
    }
}