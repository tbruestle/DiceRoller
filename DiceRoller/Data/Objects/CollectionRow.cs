﻿using System;
using System.Data;
using System.ComponentModel;

namespace DiceRoller.Data.Objects
{
    public abstract class CollectionRow : CollectionRoot, IDisposable, INotifyPropertyChanged
    {
        #region "Member Variables"
            CollectionRoot m_parentobj = null;
            public DataRow m_dr = null;
            Int32 m_index = -1;
            bool m_isdirty = false;
        #endregion
        #region "Constructors"
            public CollectionRow()
            {
            }
            public CollectionRow(CollectionRoot parentobj, DataRow dr, Int32 index)
            {
                m_parentobj = parentobj;
                m_dr = dr;
                m_index = index;
                m_isdirty = false;
            }
        #endregion
        #region "Destructors"
            ~CollectionRow()
            {
                Dispose(false);
            }
        #endregion
        #region "Properties"
            public override string TableName
            {
                get { return ParentObject.TableName; }
            }
            public override CollectionApplication Application
            {
                get { return ParentObject.Application; }
            }
            public CollectionRoot ParentObject
            {
                get { return m_parentobj; }
            }
            public bool IsDirty
            {
                get { return m_isdirty; }
            }
            public DataRow Row
            {
                get { return m_dr; }
            }
            public Int32 Index
            {
                get { return m_index; }
            }
            public virtual string IdColumnName
            {
                get { return "id"; }
            }

            public string getColumnValue(string columnname)
            {
                return m_dr[columnname].ToString();
            }
            public void setColumnValue(string columnname, string value)
            {
                if (!value.Equals(m_dr[columnname]))
                {
                    m_dr[columnname] = value;
                    ColumnChange(columnname);
                    m_isdirty = true;
                }
            }

            public virtual Int32 id
            {
                get { return Int32.Parse(getColumnValue(IdColumnName)); }
            }
        #endregion
        #region "Methods"
            public bool Update(int id, DataRow dr)
            {
                bool functionReturnValue = false;
                functionReturnValue = DataAccessModule.DataAccess.UpdateRow(tablename: m_parentobj.TableName, id: id, dr: dr);
                m_isdirty = false;
                return functionReturnValue;
            }
            public bool Revert(int id)
            {
                DataRow dr = DataAccessModule.DataAccess.GetRow(tablename: m_parentobj.TableName, id: id);
                if (dr == null)
                    return false;
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    if (m_dr.Table.Columns.Contains(dc.ColumnName))
                    {
                        m_dr[dc.ColumnName] = dr[dc.ColumnName];
                    }
                }
                m_isdirty = false;
                return true;
            }
        #endregion
        #region "IDisposable Support"
            private bool disposedValue;

            public virtual void DisposeManaged()
            {
            }

            public virtual void DisposeUnmanaged()
            {
            }
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        DisposeManaged();
                    }
                    DisposeUnmanaged();
                }
                disposedValue = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        #endregion
        #region "INotifyPropertyChanged Support"
            public event PropertyChangedEventHandler PropertyChanged;
            public void PropertyChange(string propertyname)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
                }
            }
            public virtual void ColumnChange(string columnname)
            {
                PropertyChange(columnname);
            }
        #endregion
    }
}