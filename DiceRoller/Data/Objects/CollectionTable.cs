﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ComponentModel;

namespace DiceRoller.Data.Objects
{

    public abstract class CollectionTable<T> : CollectionRoot, IDisposable, IList<T>, INotifyPropertyChanged where T : CollectionRow
    {

        #region "Member Variables"
            CollectionRoot m_parentobj = null;
            DataTable m_table = null;
            List<T> m_items = null;
            Int32 m_cnt = 0;
            string m_tablename = null;
        #endregion
        #region "Constructors"
            public CollectionTable()
            {
            }
            public CollectionTable(CollectionRoot parentobj, string tablename)
            {
                m_parentobj = parentobj;
                m_tablename = tablename;
                LoadTable();
            }
        #endregion
        #region "Destructors"
            ~CollectionTable()
            {
                Dispose(false);
            }
        #endregion
        #region "Properties"
        public override string TableName
            {
                get { return m_tablename; }
            }
            public override CollectionApplication Application
            {
                get { return m_parentobj.Application; }
            }
            public object ParentObject
            {
                get { return m_parentobj; }
            }
        #endregion
        #region "IDisposable Support"
            private bool disposedValue;

            public virtual void DisposeManaged()
            {
            }

            public virtual void DisposeUnmanaged()
            {
            }
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        DisposeManaged();
                    }
                    DisposeUnmanaged();
                    DisposeChildren();
                }
                disposedValue = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            public virtual void DisposeChildren()
            {
                if (m_items != null)
                {
                    foreach (T item in m_items)
                    {
                        item.Dispose();
                    }
                    m_items.Clear();
                    m_items = null;
                }
                if (m_table != null)
                {
                    m_table.Dispose();
                }
                m_cnt = 0;
            }
        #endregion
        #region "IList Support"
            public virtual void LoadTable()
            {
                if ((m_table != null))
                    return;
                try
                {
                    m_table = DataAccessModule.DataAccess.GetTable(tablename: TableName);
                    m_items = new List<T>();
                    m_cnt = m_table.Rows.Count;
                    for (Int32 i = 0; i <= m_cnt - 1; i++)
                    {
                        object[] m_item_args = {
                            this,
                            m_table.Rows[i],
                            i
                        };
                        T m_item = (T)Activator.CreateInstance(typeof(T), m_item_args);
                        m_items.Add(m_item);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            public System.Data.DataTable Table
            {
                get
                {
                    LoadTable();
                    return m_table;
                }
            }
            public List<T> Items
            {
                get
                {
                    LoadTable();
                    return m_items;
                }
            }
            public virtual void ClearTable()
            {
                DisposeChildren();
            }
            public T this[int index]
            {
                get { return m_items[index]; }
                set
                {
                    throw new NotImplementedException();
                }
            }
            public T ItemById(int id)
            {
                var lst = m_items.Where(f => f.id == id);
                if (lst.Count<T>() == 0)
                {
                    return null;
                }
                else
                {
                    return lst.First<T>();
                }
            }
            public int Count
            {
                get { return m_cnt; }
            }
            public bool IsReadOnly
            {
                get { return true; }
            }
            public int IndexOf(T item)
            {
                return m_items.IndexOf(item);
            }
            public void Insert(int index, T item)
            {
                throw new NotImplementedException();
            }
            public void RemoveAt(int index)
            {
                throw new NotImplementedException();
            }
            public void Add(T item)
            {
                throw new NotImplementedException();
            }
            public void Clear()
            {
                ClearTable();
            }
            public bool Contains(T item)
            {
                return m_items.Contains(item);
            }
            public void CopyTo(T[] array, int arrayIndex)
            {
                throw new NotImplementedException();
            }
            public bool Remove(T item)
            {
                throw new NotImplementedException();
            }
            public IEnumerator<T> GetEnumerator()
            {
                return m_items.GetEnumerator();
            }
            private IEnumerator IEnumerable_GetEnumerator()
            {
                return m_items.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return IEnumerable_GetEnumerator();
            }
            public bool CreateRow(int id, DataRow dr)
            {
                return DataAccessModule.DataAccess.CreateRow(tablename: TableName, id: id, dr: dr);
                ClearTable();
                LoadTable();
            }
            public bool DeleteRow(int id)
            {
                return DataAccessModule.DataAccess.DeleteRow(tablename: TableName, id: id);
                ClearTable();
                LoadTable();
            }
        #endregion
        #region "INotifyPropertyChanged Support"
            public event PropertyChangedEventHandler PropertyChanged;
            public void PropertyChange(string propertyname)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
                }
            }
            public virtual void ColumnChange(string columnname)
            {
                foreach (T itm in m_items)
                {
                    itm.PropertyChange(columnname);
                }
            }
        #endregion
    }
}