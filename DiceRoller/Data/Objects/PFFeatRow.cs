﻿using System;
using System.Data;

namespace DiceRoller.Data.Objects
{

    public class PFFeatRow : CollectionRow
    {
        #region "Constructors"
            public PFFeatRow()
            {
            }
            public PFFeatRow(CollectionRoot parentobj, DataRow dr, Int32 index) : base(parentobj, dr, index)
            {
            }
        #endregion
        #region "Properties"
            public string name
            {
                get { return getColumnValue("name"); }
                set { setColumnValue("name",value); }
            }
            public string type
            {
                get { return getColumnValue("type"); }
                set { setColumnValue("type", value); }
            }
            public string description
            {
                get { return getColumnValue("description"); }
                set { setColumnValue("description", value); }
            }
            public string prerequisites
            {
                get { return getColumnValue("prerequisites"); }
                set { setColumnValue("prerequisites", value); }
            }
            public string prerequisite_feats
            {
                get { return getColumnValue("prerequisite_feats"); }
                set { setColumnValue("prerequisite_feats", value); }
            }
            public string benefit
            {
                get { return getColumnValue("benefit"); }
                set { setColumnValue("benefit", value); }
            }
            public string normal
            {
                get { return getColumnValue("normal"); }
                set { setColumnValue("normal", value); }
            }
            public string special
            {
                get { return getColumnValue("special"); }
                set { setColumnValue("special", value); }
            }
            public string source
            {
                get { return getColumnValue("source"); }
                set { setColumnValue("source", value); }
            }
            public string fulltext
            {
                get { return getColumnValue("fulltext"); }
                set { setColumnValue("fulltext", value); }
            }
            public string teamwork
            {
                get { return getColumnValue("teamwork"); }
                set { setColumnValue("teamwork", value); }
            }
            public string critical
            {
                get { return getColumnValue("critical"); }
                set { setColumnValue("critical", value); }
            }
            public string grit
            {
                get { return getColumnValue("grit"); }
                set { setColumnValue("grit", value); }
            }
            public string style
            {
                get { return getColumnValue("style"); }
                set { setColumnValue("style", value); }
            }
            public string performance
            {
                get { return getColumnValue("performance"); }
                set { setColumnValue("performance", value); }
            }
            public string racial
            {
                get { return getColumnValue("racial"); }
                set { setColumnValue("racial", value); }
            }
            public string companion_familiar
            {
                get { return getColumnValue("companion_familiar"); }
                set { setColumnValue("companion_familiar", value); }
            }
            public string race_name
            {
                get { return getColumnValue("race_name"); }
                set { setColumnValue("race_name", value); }
            }
            public string note
            {
                get { return getColumnValue("note"); }
                set { setColumnValue("note", value); }
            }
            public string goal
            {
                get { return getColumnValue("goal"); }
                set { setColumnValue("goal", value); }
            }
            public string completion_benefit
            {
                get { return getColumnValue("completion_benefit"); }
                set { setColumnValue("completion_benefit", value); }
            }
            public string multiples
            {
                get { return getColumnValue("multiples"); }
                set { setColumnValue("multiples", value); }
            }
            public string suggested_traits
            {
                get { return getColumnValue("suggested_traits"); }
                set { setColumnValue("suggested_traits", value); }
            }
        #endregion
    }

}