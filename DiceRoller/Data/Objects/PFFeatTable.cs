﻿using System.Linq;
using System.Data;

namespace DiceRoller.Data.Objects
{

    public class PFFeatTable : CollectionTable<PFFeatRow>
    {
        #region "Constructors"
            public PFFeatTable() : base(null, "pffeat")
            {
            }
            public PFFeatTable(CollectionRoot parentobj, string tablename) : base(parentobj, tablename)
            {
            }
            public PFFeatTable(CollectionRoot parentobj) : base(parentobj, "pffeat")
            {
            }
        #endregion
        #region "Properties"
            public PFFeatRow Feat_By_Name(string name)
            {
                dynamic lst = this.Items.Where(f => f.name == name);
                if (lst.Count > 0)
                {
                    return lst.First();
                }
                else
                {
                    return null;
                }
            }
        #endregion

    }
}