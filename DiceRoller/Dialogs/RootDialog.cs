﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Text.RegularExpressions;

namespace DiceRoller.Dialogs
{
	[Serializable]
	public class RootDialog : IDialog<object>
	{
        //DiceRoller.Data.Objects.CollectionApplication m_application = new DiceRoller.Data.Objects.CollectionApplication();

        public Task StartAsync(IDialogContext context)
		{
			context.Wait(MessageReceivedAsync);

			return Task.CompletedTask;
		}
        


        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
		{
			var activity = await result as Activity;

			string input = activity.Text;

            string output = parseFunction(input);
            if (output != "")
            {
                await context.PostAsync(
                    $"{activity.From.Name} sent \"{input}\" which returned as \"{output}\"."
                );
            }

            if (output == "")
            {
                output = parsePFFeat(input);
                if (output != "")
                {
                    await context.PostAsync(output);
                }
            }


            context.Wait(MessageReceivedAsync);
        }
        private string parsePFFeat(string input)
        {
            //Start Text
            string patternStartText = "^pffeat\\s+";
            if (Regex.IsMatch(input, patternStartText))
            {
                input = Regex.Replace(input, patternStartText, "");
            }
            else
            {
                return "";
            }

            return DiceRoller.Objects.PathfinderFeats.ShowFeat(input);

            //return m_application.PFFeatTable.Feat_By_Name(input).fulltext;
        }

        private string parseInnerFunction(string input)
        {
            //Spaces
            string spaceParens = "\\s";
            while (Regex.IsMatch(input, spaceParens))
            {
                input = Regex.Replace(input, spaceParens, "");
            }

            //Parentheses
            string patternParens = "\\([^\\(\\)]*\\)";
            while (Regex.IsMatch(input, patternParens))
            {
                Match m = Regex.Match(input, patternParens);
                input = input.Substring(0, m.Index) + parseInnerFunction(input.Substring(m.Index + 1, m.Length - 2)) + input.Substring(m.Index + m.Length);

            }

            //Dice
            string patternDice = "(\\d+)d(\\d+)";
			while (Regex.IsMatch(input, patternDice))
			{
				Match m = Regex.Match(input, patternDice);

				int i1 = int.Parse(m.Groups[1].ToString());
				int i2 = int.Parse(m.Groups[2].ToString());


				int iRoll = 0;

				Random rnd = new Random();
				for (int j = 0; j < i1; j++)
				{
					iRoll += rnd.Next(1, i2+1);
				}

				input = input.Substring(0, m.Index) + iRoll.ToString() + input.Substring(m.Index + m.Length);
			}

			//Multiply/Divide
			string patternMD = "(\\d+)([\\/\\*])(\\d+)";
			while (Regex.IsMatch(input, patternMD))
			{
				Match m = Regex.Match(input, patternMD);

				int i1 = int.Parse(m.Groups[1].ToString());
				string f = m.Groups[2].ToString();
				int i2 = int.Parse(m.Groups[3].ToString());

				if (f == "/")
				{
					input = input.Substring(0, m.Index) + (i1 / i2).ToString() + input.Substring(m.Index + m.Length);
				}
				else if (f == "*")
				{
					input = input.Substring(0, m.Index) + (i1 * i2).ToString() + input.Substring(m.Index + m.Length);
				}
				else
				{
					throw new Exception("Multiply/Divide Error With Syntax");
				}
			}

			//Add/Subtract
			string patternAD = "(\\d+)([\\-\\+])(\\d+)";
			while (Regex.IsMatch(input, patternAD))
			{
				Match m = Regex.Match(input, patternAD);

				int i1 = int.Parse(m.Groups[1].ToString());
				string f = m.Groups[2].ToString();
				int i2 = int.Parse(m.Groups[3].ToString());

				if (f == "+")
				{
					input = input.Substring(0, m.Index) + (i1 + i2).ToString() + input.Substring(m.Index + m.Length);
				}
				else if (f == "-")
				{
					input = input.Substring(0, m.Index) + (i1 - i2).ToString() + input.Substring(m.Index + m.Length);
				}
				else
				{
					throw new Exception("Add/Subtract Error With Syntax");
				}
			}

			return input;
		}
		private string parseFunction(string input)
		{
			//Start Text
			string patternStartText = "^roll\\s+";
			if (Regex.IsMatch(input, patternStartText))
			{
				input = Regex.Replace(input, patternStartText, "");
			}
			else
			{
				return "";
			}

			//End Text
			string patternEndText = "\\s+[a-zA-Z]+$";
			string endText = "";
			if (Regex.IsMatch(input, patternEndText))
			{
				Match m = Regex.Match(input, patternEndText);
				endText = m.Value;
				input = input.Substring(0,m.Index);
			}

			input.Replace(" ", "");

			input = parseInnerFunction(input);

			return input + endText;
		}
	}
}