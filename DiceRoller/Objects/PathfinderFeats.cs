﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DiceRoller.Objects
{
    public static class PathfinderFeats
    {
        private static System.Data.DataTable m_dt = null;

        public static void LoadSheet()
        {
            if (m_dt != null) { return; }
            m_dt = SQLAccess.GetTablePFFeat();
        }

        public static string ShowFeat(string feat_name)
        {
            string m_return = "";
            LoadSheet();
            DataRow[] result = m_dt.Select("name ='" + feat_name.Trim() + "'");

            // Display.
            foreach (DataRow row in result)
            {
                m_return += row["fulltext"];
            }
            return m_return;
        }
    }
}