﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DiceRoller.Objects
{
    public static class SQLAccess
    {
        #region "Connection"
        static SqlConnection conn = null;
        public static string ConnectionString
        {

            get { return ConfigurationManager.AppSettings.Get("SQLConnectionString"); }
        }
        public static SqlConnection Connection
        {
            get
            {
                if (conn == null)
                {
                    conn = new SqlConnection(ConnectionString);
                }
                return conn;
            }
        }
        #endregion
        #region "CharacterClass"
        #region "Get Table"
        public static DataTable GetTablePFFeat()
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("[dbo].[P_GetTablePFFeat]");

                cmd.Connection = Connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = 0;

                //Fill the dataset
                adapter.Fill(ds);
                Connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region "Get Row"
        public static DataRow GetRowPFFeat(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand("[dbo].[P_GetRowPFFeat]");

                cmd.Parameters.Add(new SqlParameter("id", SqlDbType.Int));
                cmd.Parameters["id"].Value = id;

                SqlConnection connection = new SqlConnection(ConnectionString);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.SelectCommand.CommandTimeout = -1;

                //Fill the dataset
                adapter.Fill(ds);
                connection.Close();

                if (ds.Tables.Count == 0)
                    return null;
                if (ds.Tables[0].Rows.Count == 0)
                    return null;
                return ds.Tables[0].Rows[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion


    }
}
